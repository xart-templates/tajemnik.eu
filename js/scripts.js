jQuery.noConflict();
jQuery(document).ready(function($){

	$('html').addClass('js');

	// container height
	var height = $(window).height();
	$('.container').css({'min-height':height});
	$(window).resize(function(){
		var height = $(window).height();
		$('.container').css({'min-height':height});
	});


	// scroll to id
	$('.mod_menu-top ul li a').each(function(){
		var href = $(this).attr('href');
		$(this).attr({'data-href':href});
		$(this).removeAttr('href');
	});


	$('.mod_menu-top ul li a').click(function(){
		var datahref = $(this).attr('data-href');
		$('html,body').animate({scrollTop: $(datahref).offset().top}, 800);
		$('.mod_menu-top ul li a').parents('li').removeClass('act');
		$(this).parents('li').addClass('act');
	});

	// scroll top
	$('a[href=#]').click(function(){
		$('html, body').animate({scrollTop:0},800);
		return false;
	});


	// mod_menu-main
	$('body').click(function(){
		$('#mainmenu').addClass('menu-hide');
	});
	$('#mainmenu').each(function(){
		$(this).click(function(event){
			event.stopPropagation();
		});
		$('.menu a',this).click(function(){
			var item = $(this).parent().attr('class');
			$(this).parents('#mainmenu').removeAttr('class').addClass(item).removeClass('menu-hide');
		});
		$('.icon-cancel',this).click(function(){
			$(this).parents('#mainmenu').removeAttr('class');
		});
	});

	// file input style
	$('form.normal input[type=file]').each(function(){var userLang=(navigator.language)?navigator.language:navigator.userLanguage;var uploadtext='Choose file';if(userLang=='cs'){var uploadText='Vyberte soubor'}if(userLang=='de'){var uploadText='Datei ausw&auml;hlen'}if(userLang=='pl'){var uploadText='Wybierz plik'}if(userLang=='fr'){var uploadText='Choisir un fichier'}if(userLang=='ru'){var uploadText='Выберите файл'}if(userLang=='es'){var uploadText='Elegir archivo'}var uploadbutton='<input type="button" class="normal-button" value="'+uploadText+'" />';var inputClass=$(this).attr('class');$(this).wrap('<span class="fileinputs"></span>');$(this).parent().append($('<span class="fakefile" />').append($('<input class="input-file-text" type="text" />').attr({'id':$(this).attr('id')+'__fake','class':$(this).attr('class')+' input-file-text'})).append(uploadbutton));$(this).addClass('type-file').css('opacity',0);$(this).bind('change mouseout',function(){$('#'+$(this).attr('id')+'__fake').val($(this).val().replace(/^.+\\([^\\]*)$/,'$1'))})});

	// ie6-9 condition 
	if (document.createElement('input').placeholder==undefined){
		// placeholder
		$('[placeholder]').focus(function(){var input=$(this);if(input.val()==input.attr('placeholder')){input.val('');input.removeClass('placeholder')}}).blur(function(){var input=$(this);if(input.val()==''||input.val()==input.attr('placeholder')){input.addClass('placeholder');input.val(input.attr('placeholder'))}}).blur()
	}

	// ie6-7 condition 
	if($.browser.msie&&parseFloat($.browser.version)<8){
		$('ul li,.mod_menu-main ul.menu li a,.submenu ul li ul,.mod_custom-reference .label,.email,.home,.time,.location,.phone').prepend('<span class="before"/>');
	}

	// ie6-8 condition 
	if($.browser.msie&&parseFloat($.browser.version)<9){
		$('').addClass('pie');
	}

});